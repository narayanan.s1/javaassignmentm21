import java.util.Arrays;

public class Reversearray {

	public static void main(String[] args) {
		//Considering an Array which has elements as 11,19,15,10

		//Write a Java program to reverse array without using second array.
		
		int[] a = {11,19,15,10};
	   
	   for(int i = 0; i < a.length / 2; i++)
	  {
	    int temp = a[i];
	    a[i] = a[a.length - i - 1];
	    a[a.length - i - 1] = temp;
	  }
	    System.out.println("Reverse array : "+Arrays.toString(a));
	 }


	

}
