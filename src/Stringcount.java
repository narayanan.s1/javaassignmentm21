
public class Stringcount {
	
	//Write a Java program to count the number of characters and digits in a String "Hello123andWelcome0"
	
	
		public static void main(String[] args) {
		    String s="Hello123andWelcome0";
		    int char_count=0;
		    int digit_count=0;
		    for(int i=0;i<s.length();i++)
		    {
		        char ch = s.charAt(i);
		        if(ch>='0'&&ch<='9')
		        {
		            digit_count++; 
		        }
		        else 
		        {
		            char_count++; 
		        }
		    }
		    System.out.println("the number of character in given string is:"+char_count);
		    System.out.println("the number of digit in given string is:"+digit_count);
		}
		}
	